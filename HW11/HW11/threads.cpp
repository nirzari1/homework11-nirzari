#include "threads.h"
#include <iostream>
#include <thread>
#include <ctime>
void I_Love_Threads()
{
	std::cout << "I Love Threads (I don't yoel)" << std::endl;
}
void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	std::thread t2(I_Love_Threads);
	t2.join();
	t1.join(); // calls threads and prints twice
}
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	bool notPrime = false;
	for (int i = begin; i < end; i++)
	{
		for (int j = 2; j <= i / 2 && !notPrime; j++)
		{
			if (i % j == 0)
			{
				notPrime = true; // not prime
			}
		}
		if (!notPrime)
		{
			primes.push_back(i); // adds to the end of the vector
		}
		notPrime = false; // reset var
	}
	printVector(primes); // calls print vector
}
void printVector(std::vector<int> primes)
{
	for (unsigned int i = 0; i < primes.size(); i++)
	{
		std::cout << primes.at(i) << std::endl; // prints vector
	}
}
std::vector<int> callGetPrimes(int begin, int end)
{
	std::clock_t start;
	double duration;
	start = std::clock(); // timer start
	std::vector<int> primes;
	std::thread thisThread(getPrimes, std::ref(begin), std::ref(end), std::ref(primes)); // calls the fnc with threads
	thisThread.join();
	std::cout << "Time taken: " << (std::clock() - start ) / (double)CLOCKS_PER_SEC << " seconds" << std::endl; // prints time taken
	return primes;
}
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	bool notPrime = false;
	bool closeHere = false;
	if (!file.is_open())
	{
		closeHere = true;
		file.open("data.txt"); // opens file if no func called this function
	}
	for (int i = begin; i < end; i++)
	{
		for (int j = 2; j <= i / 2 && !notPrime; j++)
		{
			if (i % j == 0)
			{
				notPrime = true; // if it gets here its not prime
			}
		}
		if (!notPrime)
		{
			file << "Prime Number: " << i << std::endl; // prints in file the prime number if found
		}
		notPrime = false; // resets
	}
	if (closeHere)
	{
		file.close(); // if another fnc is calling this fnc with an opened file
	}
}
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::clock_t start;
	double duration;
	start = std::clock();
	int range = end - begin;
	range /= N; // gets range needs to be divided to
	int sendToParamEnd = 0;
	int sendToParamBegin = begin;
	std::thread* tArr = new std::thread[N]; //creates thread arr N size
	std::ofstream file;
	file.open(filePath); // opens file
	for (int i = 0; i < N; i++)
	{
		sendToParamEnd = end - (range *(N - i - 1)); // increases range everytime (end param)
		tArr[i] = std::thread(writePrimesToFile, std::ref(sendToParamBegin), std::ref(sendToParamEnd), std::ref(file)); // sends params and adds a thread to arr
		tArr[i].join();
		sendToParamBegin += range; // increases range everytime (begin param)
	}
	delete[] tArr; // delets arr, closes file
	file.close();
	std::cout << "Time taken: " << (std::clock() - start) / (double)CLOCKS_PER_SEC << " seconds" << std::endl; // prints time taken
}